﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Net.Mime;
using System.Windows.Controls;
using System.Windows.Media;

namespace HB.Model
{
    public class Furniture
    {
        private string name;
        public string Name { get => name; set => name = value; }


        private uint trX;
        public uint TrX { get => trX; set => trX = value; }

        private uint trY;
        public uint TrY { get => trY; set => trY = value; }

        public Furniture(uint trX, uint trY, string name)
        {
            this.trX = trX;
            this.trY = trY;
            this.name = name;
        }


    }
}
