﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HB
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var app = new View.HomeBuilder();
            app.DataContext = new ViewModel.HBViewModel();
            app.ShowDialog();
        }
    }
}
