﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HB.View
{
    public partial class HomeBuilder : Window
    {
        uint transform = 0;
        public HomeBuilder()
        {
            InitializeComponent();
            Canvas.SetLeft(backGroundImg, 0 * 85);
            Canvas.SetBottom(backGroundImg, 0 * 49);
            Canvas.SetBottom(furImg, 1 * 49);
            Canvas.SetBottom(furImg, 0 * 49);
        }
        private void OnSceneLoaded(object sender, System.Windows.RoutedEventArgs e) { }
        protected void OnValueChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            furImg.Visibility = Visibility.Visible;
            Canvas.SetBottom(furImg, sliderX.Value * 49);
            backGroundImg.Visibility = Visibility.Visible;
        }
        protected void OnValueChanged1(object sender, System.Windows.RoutedEventArgs e)
        {
            sliderX.Value += 0;
            furImg.Visibility = Visibility.Visible;
            Canvas.SetLeft(furImg, sliderY.Value * 85);
            backGroundImg.Visibility = Visibility.Visible;
        }
        private void RotateButtonClick(object sender, RoutedEventArgs e) {

            transform += 90;
            if (transform == 360)
            {
                transform = 0;
            }
            RotateTransform trs = new RotateTransform(transform);
            furImg.RenderTransform = trs;
        }

        private void PasteButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            RenderTargetBitmap rtb = new RenderTargetBitmap(2000,
        2000, 0d, 0d, System.Windows.Media.PixelFormats.Default);
            rtb.Render(canv);

            var crop = new CroppedBitmap(rtb, new Int32Rect(0, 0, 1050, 640));

            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(crop));


            using (var fs = System.IO.File.OpenWrite("Saved.png"))
            {
                pngEncoder.Save(fs);
            }
            backGroundImg.Source = LoadBitmapImage("Saved.png");
        }

        public static BitmapImage LoadBitmapImage(string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                return bitmapImage;
            }
        }
    }       
}
