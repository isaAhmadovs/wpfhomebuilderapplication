﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HB.Model;
using HB.View;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Media;

namespace HB.ViewModel
{
    class HBViewModel : ViewModelBase
    {
        public string FilePath { get; private set; }
        public string FileDest { get; private set; }
        private HomeBuilder hb { get; }

        private string image;
        public string thisImage { get => image; set => Set(ref image, value); }

        private ObservableCollection<Furniture> furnitures = new ObservableCollection<Furniture>{
            new Furniture(0, 0, "Wall"),
            new Furniture(0, 0, "Sofa"),
            new Furniture(0, 0, "Table"),
            new Furniture(0, 0, "Chair"),
            new Furniture(0, 0, "TV"),
            new Furniture(0, 0, "Plate"),
            new Furniture(0, 0, "Sink"),
            new Furniture(0, 0, "Toilet"),
            new Furniture(0, 0, "Bath"),
            new Furniture(0, 0, "Shower"),
            new Furniture(0, 0, "Door"),
            new Furniture(0, 0, "Window"),
            new Furniture(0, 0, "Crane")
        };
        public ObservableCollection<Furniture> Furnitures { get => furnitures; set => Set(ref furnitures, value); }

        private Furniture selected = null;
        public Furniture Selected
        {
            get => selected;
            set
            {
                Set(ref selected, value);
                thisImage = $@"..\Source\{Selected.Name}.png";
            }
        }

        private RelayCommand saveCommand;
        public RelayCommand SaveCommand
        {
            get => saveCommand ?? (saveCommand = new RelayCommand(
                () =>
                {
                    var source = @"Saved.png";

                    var dest = new SaveFileDialog();
                    if (dest.ShowDialog().GetValueOrDefault())
                    {
                        File.Copy(source, $"{dest.FileName}.png");
                    }
                }
                ));
        }
    }
}
